%define with_python2    0
%define with_python3    1
%define with_tools      1

Name:           libbytesize
Version:        2.7
Release:        1
Summary:        A library for working with sizes in bytes
License:        LGPLv2+
URL:            https://github.com/storaged-project/libbytesize
Source0:        https://github.com/storaged-project/libbytesize/releases/download/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc gmp-devel mpfr-devel pcre-devel gettext-devel libxslt pcre2-devel
%if %{with_python2}
BuildRequires: python2-devel python2-polib python2-pocketlint python2-six
%else
Obsoletes: python2-libbytesize
%endif
%if %{with_python3}
BuildRequires: python3-devel python3-polib python3-pocketlint python3-six glibc-all-langpacks
%endif
BuildRequires: gtk-doc


%description
The goal of this project is to provide a tiny library that would
facilitate the common operations with sizes in bytes.

%package        devel
Summary:        Files for %{name} development
Requires:       %{name} = %{version}-%{release}
%description    devel
Files for %{name} development

%if %{with_python3}
%package -n python3-bytesize
Summary: Python 3 bindings for libbytesize
Requires: %{name} = %{version}-%{release}
Requires: python3-six
%description -n python3-bytesize
Python 3 bindings for libbytesize.
%endif

%if %{with_python2}
%package -n python2-bytesize
Summary: Python 2 bindings for libbytesize
Requires: %{name} = %{version}-%{release}
Requires: python2-six
%description -n python2-bytesize
Python 2 bindings for libbytesize.
%endif

%if %{with_tools}
%package tools
Summary: Various nice tools based on libbytesize
Requires: python3-bytesize = %{version}-%{release}

%description tools
Variousnicetoolsbasedonlibbytesize, in particular the calculator
for doing calculations with storage sizes.
%endif

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install
find %{buildroot} -type f -name "*.la" -delete
%find_lang %{name}

%check
make check

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%license LICENSE
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%if %{with_python2}
%files -n python2-bytesize
%defattr(-,root,root)
%{_libdir}/python2*/site-packages/*
%else
%exclude %{_libdir}/python2*/site-packages/*
%endif

%if %{with_python3}
%files -n python3-bytesize
%defattr(-,root,root)
%{_libdir}/python3*/site-packages/*
%endif

%files help
%doc ABOUT-NLS README.md
%{_datadir}/gtk-doc/html/*

%if %{with_tools}
%files tools
%{_bindir}/bscalc
%{_mandir}/man1/bscalc.1*
%endif

%changelog
* Thu Jan 19 2023 gaoruoshu <gaoruoshu@huawei.com> - 2.7-1
- update version to 2.7

* Tue Jul 12 2022 panxiaohe <panxh.life@foxmail.com> - 2.6-2
- enable check test suite

* Tue Nov 30 2021 panxiaohe<panxiaohe@huawei.com> - 2.6-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 2.6

* Thu Jul 22 2021 panxiaohe <panxiaohe@huawei.com> - 2.4-2
- remove unnecessary BuildRequires: gdb

* Tue Sep 1 2020 wangchen <wangchen137@huawei.com> - 2.4-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 2.4

* Sat Jul 25 2020 yang_zhuang_zhuang <yangzhuangzhuang1@huawei.com> - 2.3-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 2.3

* Sat Mar 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.4-8
- fixbug in remove python2 support for package

* Sat Mar 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.4-7
- fixbug in self-building

* Sat Feb 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.4-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove python2 support for package

* Fri Feb 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.4-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add build requires of python2-devel if with python2

* Wed Nov 20 2019 fangyufa<fangyufa1@huawei.com> - 1.4-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build problem for x86_64

* Tue Sep 24 2019 shenyangyang<shenyangyang4@huawei.com> - 1.4-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise help package

* Wed Aug 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.4-2
- Package init
